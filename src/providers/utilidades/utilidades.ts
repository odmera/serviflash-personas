import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';


/**
 * proveedor que se encarga de gestionar las ultidades de la app
 * @author OM
 */
@Injectable()
export class UtilidadesProvider {

  constructor(public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public geolocation: Geolocation) {
  }

  doAlert(title, mensaje, buttonText) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: mensaje,
      buttons: [buttonText]
    });
    return alert;
  }

  presentarLoading(mensaje: string) {
    let loading = this.loadingCtrl.create({
      content: mensaje
    });
    return loading;
  }

  presentarToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000
    });
    toast.present();
    return toast;
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }

  /** 
   * metodo que permite obtener la posicion actual del usuario.
  */
  obtenerPosicion() {
    this.geolocation.getCurrentPosition().then((position) => {
      this.presentarToast(" latitude " + position.coords.latitude + 
                          " longitude " + position.coords.longitude);
    }, (err) => {
      console.log(err);
      this.presentarToast("error " + err);
    });
  }

}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { UtilidadesProvider } from '../utilidades/utilidades';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiConfig } from '../../configuracion/config';
import { Usuario } from '../../modelo/usuario';


/**
 * proveedor que se encarga de la autenticacion de usuarios. OM
 */

@Injectable()
export class AutenticacionProvider {


  public displayName;

  constructor(private afAuth: AngularFireAuth, 
             private fb: Facebook, 
             private platform: Platform,
             private _util: UtilidadesProvider,
             public http: HttpClient) {

    afAuth.authState.subscribe(user => {
      if (!user) {
        this.displayName = null;
        return;
      }
      this.displayName = user.displayName;
    });
  }

  /**
   * metodo que permite hacer login con facebook OM
   */
  async loginFacebook(): Promise<Usuario>{
    try {
      if (this.platform.is('cordova')) { //desde el disposivo 
          let response =  await  this.fb.login(['email', 'public_profile']);
          if (response.status == 'connected') {
           
            //se registra el usuario el firebase  las credenciales de facebook.
            const facebookCredential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
            let successFirebase = await  firebase.auth().signInWithCredential(facebookCredential)
            
            if (successFirebase){
              //se obtienen los datos del usuario de facebook
              let user = await this.obtenerDatosUsuarioFacebook();

              let usuario = new Usuario();
              usuario.nombreCompleto = user.name;
              usuario.correoUsuario = user.email;
              usuario.token = response.authResponse.accessToken;

              //se envie el usuario a la api.
              //let respuesta = await this.agregarUsuarios(user); TODO: PENDIENTE
              return usuario;
            }
          }           
      }
      else { //desde el navegador web
        let datos = await  this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
        console.log(datos.additionalUserInfo.profile);

        let usuario = new Usuario();
        usuario.correoUsuario = datos.additionalUserInfo.profile.email;

            
        //se envie el usuario a la api.
        //let respuesta = this.agregarUsuarios(user);
        return usuario;
        
      }
    } catch (error) {
       new error;
    }
  }



  /**
   * metodo que permite obtener datos de los usuario de facebook
   */
 async obtenerDatosUsuarioFacebook() {

    try {
  
      let datos = await this.fb.api('/me?fields=id,name,email,first_name,picture,last_name,gender', ['public_profile', 'email'])
  
      let user = {
        name: datos.name, username: datos.email,
        email: datos.email, phone: '', website: '',
        address: {
          street: '', suite: '', city: '',
          zipcode: '', geo: { lat: '', lng: '' }
        },
        company: { name: '', bs: '', catchPhrase: '' }
      };

      return user;
  
    } catch (error) {
      new error;
    }
  }

  cerrarSesionFirebase() {
    this.afAuth.auth.signOut();
  }

  /**
   * ejemplo para agregar el usuario atenticado a la api. TODO: ESQUE SE DEBE MODIFICAR
   * @param data 
   */
  agregarUsuarios(data) {
    return new Promise((resolve, reject) => {
      this.http.post(ApiConfig.url + '/users', JSON.stringify(data), {
        headers: new HttpHeaders().set('Authorization', 'my-auth-token'),
        params: new HttpParams().set('id', '3'),
        })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  async loginGoogle(){
  
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
  
      await firebase.auth().signInWithRedirect(provider);
  
      let result = await firebase.auth().getRedirectResult();
  
      var token = result.credential.accessToken;
      var user = result.user;
  
      let usuario = new Usuario();
      usuario.nombreCompleto = user.name;
      usuario.correoUsuario = user.email;
      usuario.token = result.credential.accessToken;
      return usuario;
    } catch (error) {
      new error;
    }

  }

}

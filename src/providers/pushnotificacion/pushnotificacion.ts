import { Injectable } from '@angular/core';
import { OneSignal, OSNotification } from '@ionic-native/onesignal';
import { Platform, AlertController } from "ionic-angular";
import { UtilidadesProvider } from '../utilidades/utilidades';

@Injectable()
export class PushnotificacionProvider {

  constructor(private oneSignal: OneSignal,
    private platform: Platform,
    public _util: UtilidadesProvider,
    private alertCtrl: AlertController) {
    console.log('Hello PushnotificacionProvider Provider');
  }

  public iniciarPushNotificaciones() {
    if (this.platform.is('cordova')) {

      this.oneSignal.startInit('e868aaa9-b29b-4a2f-bacf-c8cbb37cd1b6', '717062998084');

      this.oneSignal.getIds().then((dato) => {
        setTimeout(() => {
          this._util.presentarToast("dato.pushToken " + dato.pushToken);
        }, 500);

        setTimeout(() => {
          this._util.presentarToast("dato.userId " + dato.userId);
        }, 3000);
      });

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

      //aqui ya sabes que al usuario le llego la notificacion
      this.oneSignal.handleNotificationReceived().subscribe(jsonData => {

        console.log("respuesta ", JSON.stringify(jsonData));

        let alert = this.alertCtrl.create({
          title: "Serviflash",
          subTitle: "Serviflash push",
          buttons: ['OK']
        });
        alert.present();
      });

      //cada vez que se le da click a una notificacion
      this.oneSignal.handleNotificationOpened().subscribe(() => {
        this._util.presentarToast("Notificacion abierta.");
      });
      this.oneSignal.endInit();
    }
    else {
      console.warn("Las push solo son para el dispositivo movil");
    }

  }

}

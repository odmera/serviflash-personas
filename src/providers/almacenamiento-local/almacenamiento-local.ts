import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { datosAL } from '../../modelo/datosAL';

/**
 * proveedor para gestionar almacenamiento local (OM)
 * Cuando se ejecuta en un contexto de aplicación nativa, Storage priorizará el uso de SQLite, 
   ya que es una de las bases de datos más estables y ampliamente utilizadas, 
   y evita algunas de las trampas de cosas como localstorage e IndexedDB, 
   como el sistema operativo que decide eliminar Tales datos en situaciones de bajo espacio en disco.

   Cuando se ejecuta en la Web o como una aplicación Web progresiva, Storage intentará utilizar 
   IndexedDB, WebSQL y localstorage, en ese orden.
 */
@Injectable()
export class AlmacenamientoLocalProvider {


  public correoUsuario: string;

  constructor(private storage: Storage) {
  }

  /**
   * metodo que permirte guardar datos en  almacenamiento local (OM)
   */
  guardarDatosAL(datosal: datosAL): Promise<boolean> {
    let promesa = new Promise<boolean>((resolve, reject) => {
      //se almacenan los valores globlamente antes de guardarlos en el almacenamiento local
      this.correoUsuario = datosal.correoUsuario;
      this.storage.set('datosSesion', datosal);
      resolve(true);
    });
    return promesa;
  }


  obtenerDatosSesion(): Promise<datosAL> {

    let promesa = new Promise<datosAL>((resolve, reject) => {
      this.storage.get('datosSesion').then((datosSesion: any) => {
        if (datosSesion != null && datosSesion) {
          let datosal = new datosAL();
          datosal.correoUsuario = datosSesion.correoUsuario;
          resolve(datosal);
        }
        else {
          resolve(null);
        }
      });

    });
    return promesa;
  }

  /**
   * metodo que permite limpar TODO el almacenamiendo local (OM)
   */
  limpiarTodoAL(): void {
    this.storage.clear().then(() => {
      console.log('se limipio todo el almacenamiendo local');
    });
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';


@IonicPage(
{
    name: 'registro'
})
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  formulario: FormGroup;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public _util: UtilidadesProvider) {

    this.formulario = formBuilder.group({
      txtNombreCompleto: ['', Validators.required],
      txtCorreo: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
      txtClave: ['', Validators.required],
      txtTelefono: ['', Validators.required],
      txtPais: ['', Validators.required],
      txtCiudad: ['', Validators.required],
      txtTerminosCondiciones: [false, Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  volverAtras() {
    this.navCtrl.pop();
  }

  registrarUsuario(){
    this._util.presentarToast("En desarrollo.");
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';

/**
 * Generated class for the RecuperarContrasenaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
    name: 'recuperar'
})
@Component({
  selector: 'page-recuperar-contrasena',
  templateUrl: 'recuperar-contrasena.html',
})
export class RecuperarContrasenaPage {

  formulario: FormGroup;
  num1:string
  num2: string
  num3: string
  num4: string
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public _util: UtilidadesProvider) {
    this.formulario = this.formBuilder.group({
      txtCorreo: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecuperarContrasenaPage');
  }

  cambiarContrasena(){
    this.navCtrl.push('cambiar');
  }

  enviarCodigoNuevo(){
    this._util.presentarToast("En desarrollo.");
  }

  volverAtras(){
    this.navCtrl.pop();
  }

}

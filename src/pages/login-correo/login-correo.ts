import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
import { TabsPage } from '../tabs/tabs';
import { datosAL } from '../../modelo/datosAL';
import { AlmacenamientoLocalProvider } from '../../providers/almacenamiento-local/almacenamiento-local';

/**
 * page que permite a las personas iniciar sesion con correo electronico
 * @author OM
 */
@IonicPage()
@Component({
  selector: 'page-login-correo',
  templateUrl: 'login-correo.html',
})
export class LoginCorreoPage {

  formularioLogin: FormGroup;
  private guardarFormulario: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public formBuilder: FormBuilder,
              public _util: UtilidadesProvider,
              private _almacenamientoLocalProvider: AlmacenamientoLocalProvider,) {

    this.formularioLogin = this.formBuilder.group({
      txtCorreo: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
      txtClave: ['', Validators.compose([Validators.required])],
    });

    this.formularioLogin.valueChanges.subscribe((v) => {
      this.guardarFormulario = this.formularioLogin.valid;
    }); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginCorreoPage');
  }

  volverAtras() {
    this.navCtrl.pop();
  }

  async loginCorreo() {

    let correo: string = this.formularioLogin.value.txtCorreo;
    let clave: string = this.formularioLogin.value.txtClave;

    if (correo == null){
      this._util.presentarToast("Datos no validos ")
      return;
    }
    if (clave == null) {
      this._util.presentarToast("Datos no validos ")
      return;
    }

    let loader = this._util.presentarLoading("Iniciando...");
    loader.present();

    //guardo los datos en la sesion
    let datosal = new datosAL()
    datosal.correoUsuario = correo;
    let almacenamientoCorrecto: boolean = await this._almacenamientoLocalProvider.guardarDatosAL(datosal);

    if (!almacenamientoCorrecto) {
      throw ("Con el almacenamiento local");
    }

    //se suscribe el usuario a los push
    if (window["plugins"] != null && window["plugins"].OneSignal != null) {
      window["plugins"].OneSignal.setSubscription(true);
    }
    this._util.presentarToast("usuario " + correo);
    this._util.obtenerPosicion();
    this.navCtrl.setRoot(TabsPage);
    loader.dismiss();

  }


  irRegistrarUsuario(){
    this.navCtrl.push("registro");
  }

  irRestablecerContrasena(){
    this.navCtrl.push("recuperar");
  }

}

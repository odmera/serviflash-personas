import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';

@IonicPage({
  name: 'cambiar'
})
@Component({
  selector: 'page-cambiar-contrasena',
  templateUrl: 'cambiar-contrasena.html',
})
export class CambiarContrasenaPage {

  formulario: FormGroup;

  constructor(public navCtrl: NavController, 
             public navParams: NavParams,
             public formBuilder: FormBuilder,
             public _util: UtilidadesProvider) {

    this.formulario = this.formBuilder.group({
      txtClave: ['',Validators.required],
      txtClaveRepetida: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CambiarContrasenaPage');
  }

  cambiarContrasena(){
    this._util.presentarToast("En desarrollo.");
  }
  volverAtras() {
    this.navCtrl.pop();
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AutenticacionProvider } from '../../providers/autenticacion/autenticacion';
import { TabsPage } from '../tabs/tabs';
import { datosAL } from '../../modelo/datosAL';
import { AlmacenamientoLocalProvider } from '../../providers/almacenamiento-local/almacenamiento-local';
import { UtilidadesProvider } from '../../providers/utilidades/utilidades';
//import { LoginCorreoPage } from '../login-correo/login-correo';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _autenticacionProvider: AutenticacionProvider,
              private _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
              private _util: UtilidadesProvider,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

 async loginFacebook(){

    try {
      let usuario = await this._autenticacionProvider.loginFacebook();

      if (usuario) {
        //guardo los datos en la sesion
        let datosal = new datosAL()
        datosal.correoUsuario = usuario.correoUsuario;
        datosal.token = usuario.token;
        let almacenamientoCorrecto: boolean = await this._almacenamientoLocalProvider.guardarDatosAL(datosal);

        if (!almacenamientoCorrecto) {
          throw ("Con el almacenamiento local");
        }

        //se suscribe el usuario a los push
        if (window["plugins"] != null && window["plugins"].OneSignal) {
          window["plugins"].OneSignal.setSubscription(true);
        }
        this._util.presentarToast("usuario " + usuario.correoUsuario);
        this._util.obtenerPosicion();
        this.navCtrl.setRoot(TabsPage);
      }
    } catch (error) {
      this._util.presentarToast("error " + error)
    }
  
  }
  async loginGoogle() {
    try {
      let usuario = await this._autenticacionProvider.loginGoogle();
      if (usuario) {
        //guardo los datos en la sesion
        let datosal = new datosAL()
        datosal.correoUsuario = usuario.correoUsuario;
        datosal.token = usuario.token;
        let almacenamientoCorrecto: boolean = await this._almacenamientoLocalProvider.guardarDatosAL(datosal);

        if (!almacenamientoCorrecto) {
          throw ("Con el almacenamiento local");
        }

        //se suscribe el usuario a los push
        if(window["plugins"] != null && window["plugins"].OneSignal) {
           window["plugins"].OneSignal.setSubscription(true);
        }
        this._util.presentarToast("usuario " + usuario.correoUsuario);
        this._util.obtenerPosicion();
        this.navCtrl.setRoot(TabsPage);
      }

    } catch (error) {
      this._util.presentarToast("error " + error)
    }

  }

  public loginCorreo() {
    this.navCtrl.push('LoginCorreoPage');
  }

  public politicas(){

  }

  public olvidoContrasena(){
    
  }

}

export class Users {
    public idUsers?: number;
    public nombre: string;
    public segundo_nombre?: string;
    public apellido: string;
    public segundo_apellido?: string;
    public email: string;
    public password: string;
    public fb_token: string
    public img_profile?: string;
    public idType_user: number;
    public idCuenta_bancaria?: number;
    public isEnable :boolean
    constructor() { 
        
    }

}
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation'

import { LoginPage } from '../pages/login/login';
import { LoginCorreoPage } from '../pages/login-correo/login-correo';
import { UtilidadesProvider } from '../providers/utilidades/utilidades';
import { firebaseConfig } from '../configuracion/config';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { Facebook } from '@ionic-native/facebook';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AlmacenamientoLocalProvider } from '../providers/almacenamiento-local/almacenamiento-local';
import { IonicStorageModule } from '@ionic/storage';
import { PushnotificacionProvider } from '../providers/pushnotificacion/pushnotificacion';

import { OneSignal } from '@ionic-native/onesignal';
import { PedidoProvider } from '../providers/pedido/pedido';
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: "Atrás"
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UtilidadesProvider,
    AutenticacionProvider,
    Facebook,
    HttpClientModule,
    AlmacenamientoLocalProvider,
    PushnotificacionProvider,
    OneSignal,
    Geolocation,
    PedidoProvider
  ]
})
export class AppModule {}

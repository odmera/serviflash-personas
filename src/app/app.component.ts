import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
//import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { LoginCorreoPage } from '../pages/login-correo/login-correo';
import { AlmacenamientoLocalProvider } from '../providers/almacenamiento-local/almacenamiento-local';
import { UtilidadesProvider } from '../providers/utilidades/utilidades';
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { datosAL } from '../modelo/datosAL';
import { PushnotificacionProvider } from '../providers/pushnotificacion/pushnotificacion';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  @ViewChild(Nav) nav: Nav;

  paginas: MenuItem[] = [
    { title: 'Inicio', component: LoginCorreoPage, icon: 'home' }
  ]

  constructor( 
              public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              private menuCrtl: MenuController,
              public _auntenticacionProvider: AutenticacionProvider,
              public _almacenamientoLocalProvider: AlmacenamientoLocalProvider,
              public _util: UtilidadesProvider,
              public _pushnotificacionProvider:PushnotificacionProvider) {
    this.initializeApp();
  }

  async initializeApp() {

    let datosAl: datosAL = await this._almacenamientoLocalProvider.obtenerDatosSesion();
    if(datosAl && datosAl.correoUsuario) {
      this._almacenamientoLocalProvider.correoUsuario = datosAl.correoUsuario;
      this.rootPage = TabsPage;
    } else {
      this.rootPage = 'LoginPage';
    }
    this.platformReady()
  }


  platformReady(): void {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#f53d3d');
      this.splashScreen.hide();
      this._pushnotificacionProvider.iniciarPushNotificaciones();
    });
  }

  abrirPagina(pagina: any) {
    this.nav.setRoot('LoginCorreoPage');
    this.menuCrtl.close();
  }

  async salir() {
    try {
      this._almacenamientoLocalProvider.limpiarTodoAL();
      let loader = this._util.presentarLoading("Saliendo...");
      await loader.present()
      await this._auntenticacionProvider.cerrarSesionFirebase();
      this.menuCrtl.close();
      //se dessuscribe el usuario a los push
      if (window["plugins"] != null && window["plugins"].OneSignal != null) {
        window["plugins"].OneSignal.setSubscription(false);
      }
      await this.nav.setRoot('LoginPage');
      loader.dismiss();
    } catch (error) {
      this._util.presentarToast("error " + error)
    }
    
  }
}

export interface MenuItem {
  title: string;
  component: any;
  icon: string;
}
